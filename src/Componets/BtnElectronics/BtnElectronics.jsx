import React from 'react';
import './BtnElectronics.scss'
import electron from '../../Images/upc.png'
const BtnElectronics = () => {
    return ( 

        <div className="btnelectronics">
             {/* <span className="spanbtncompu fas fa-atom"></span> */}
             <img className="electron" src={electron} alt=""/>
             
        </div>
     );
}
 
export default BtnElectronics;
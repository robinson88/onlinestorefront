import React from "react";
import { useHistory } from "react-router-dom";
import { Link } from 'react-router-dom';
import './AuthButton.scss'

export default function AuthButton(props) {
    let history = useHistory();
    const user = JSON.parse(localStorage.getItem('usuario'));


    const signOut = () => {
        localStorage.removeItem('token');
        localStorage.removeItem('usuario');
        props.fnSetIsLogged(false);
        history.push("/");
    }


    return props.isLogged ? (

        <Link to="/" className="nav-link">
            <li className="nav-item" onClick={signOut}>
                <span className="fas fa-power-off logoout " > </span><span>  Logout</span>
            </li>
        </Link>
    ) : (
        <Link  to='/login' className ="nav-link">
        <li className="nav-item">
        <span className="fas fa-power-off logooutr" > </span> <span >Login</span>
        </li>
    </Link>

        );
}
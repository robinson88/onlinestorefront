import React from 'react';
import 'bootstrap/package.json'
import './NavComponent.scss'
import { Link } from 'react-router-dom';
import logo from '../../Images/myt.png'
const NavComponent = () => {


    return (

        
            <nav className="navbar navbar-expand-lg navbar-dark" >
                <a className="navbar-brand" href="/">
                    <img src={logo} width="70" height="30" alt="" loading="lazy" />
                </a> 
                <a className="navbar-brand " href="/" tabindex="0"><span className="nameinc">MyTecnologic</span></a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <Link className="nav-link" to='/'>
                            <li className="nav-item">
                                <span> Home</span>
                            </li>
                        </Link>
                        <Link  to='/login' className ="nav-link">
                            <li className="nav-item">
                            <span class="fas fa-power-off logooutr2" > </span> <span >Login</span>
                            </li>
                        </Link>
                        <Link to='/register' className ="nav-link" >
                            <li className="nav-item">
                                <span className="fas fa-user "> <span>My Acount</span></span>
                            </li>
                        </Link>

                    </ul>
                    <form className="form-inline my-2 my-lg-0">
                        <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                        <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                    {/* <Link to="/cesta" className="nav-link"> 
                        
                        <span className="spancesta">Cesta</span><span class="fas fa-cart-plus cesta"></span>
                
                    </Link> */}
                </div>
            </nav>
       
    );
}

export default NavComponent;
import React from 'react';
import './HomeAppliances.scss'

import tv from '../../Images/tv.png'
const HomeAppliances = () => {
    return ( 
        <div className="btnhomeapp">
             {/* <span className="spanbtncompu fas fa-tv"></span> */}
             <img className="tv" src={tv} alt=""/>
        </div>
     );
}
 
export default HomeAppliances;
import React from 'react';
import './Start.scss'

const Start = () => {
    return ( 

        <div className="principalstart">
            <div className="divstart">
                <span className="textstart">
                    START
                </span>
            </div>
        </div>
     );
}
 
export default Start;
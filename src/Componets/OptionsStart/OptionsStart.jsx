import React from 'react';
import { Planet } from 'react-planet';
import Start from '../Start/Start';
import { Link } from 'react-router-dom';
import BtnElectronics from '../BtnElectronics/BtnElectronics';
import BtnToys from '../BtnToys/BtnToys';
import BtnLoginInit from '../BtnLoginInit/BtnLoginInit';
import BtnRegisterInit from '../BtnRegisterInit/BtnRegisterInit';
const OptionsStart = () => {
    return ( 
        <div className="optionstartprincipal">

< Planet
                    orbitStyle={defaultStyle => ({
                        ...defaultStyle,
                        borderWidth: 4,
                        borderStyle: 'dashed',
                        borderColor: '# 6f03fc',
                    })}
                    dragablePlanet
                    dragRadiusPlanet={20}
                    rebote
                    centerContent={<Start></Start>}
                     hideOrbit
                    autoClose
                    orbitRadius={90}
                    bounceOnClose
                    rotation={180}
                    // the bounce direction is minimal visible
                    // but on close it seems the button wobbling a bit to the bottom
                    bounceDirection="BOTTOM"
                >
                    <Link to="/login">
                        <BtnLoginInit></BtnLoginInit>
                    </Link>
                    <Link to="/register">
                        <BtnRegisterInit></BtnRegisterInit>
                    </Link>
                    <Link to="/home">
                        <BtnToys ></BtnToys>
                    </Link>
                   


                </Planet>

        </div>
     );
}
 
export default OptionsStart;
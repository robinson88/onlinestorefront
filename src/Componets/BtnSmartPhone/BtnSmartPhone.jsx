import React from 'react';
import './BtnSmartPhone.scss'
import phone from '../../Images/smartphone.png'
const BtnSmartPhone = () => {
    return ( 
        <div className="btnsmartphone">
             {/* <span class="fas fa-mobile-alt"></span> */}
             <img className="phone" src={phone} alt=""/>
        </div>
     );
}
 
export default BtnSmartPhone;
import React from 'react';
import './Reloading.scss'

const Reloading = () => {
    return (
        <button class="btn btn-outline-success" type="button" disabled>
  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
  <span class="sr-only">Loading...</span>
</button>
    );
}

export default Reloading;

import React, { useState, useEffect } from 'react';
import './NavComponentLogued.scss'
import { Link } from 'react-router-dom';
import AuthButton from '../AuthButton/AuthButton';
import { API } from "../../services/Api";
import { useForm } from "react-hook-form";
import chico from '../../Images/chico1.png'
import logo from '../../Images/myt.png'

const NavComponentLogued = () => {
    const [infosearch, setInfosearch] = useState('')
    const [isLogged, setIsLogged] = useState(!!localStorage.getItem('token'));
    const [datoproducts, setDatoproducts] = useState([])
    const [datofiltered, setDatofiltered] = useState([])
    const [categoria,setCategoria]=useState('')
    const { register, handleSubmit, watch, errors } = useForm();

    const usuario = JSON.parse(localStorage.getItem('usuario'));
    useEffect(() => {
        API.get(process.env.REACT_APP_BACK_URL + "uploadproduct").then((res) => {
            setDatoproducts(res.data);
        });
    }, []);

    const filterProducts = (filterValues) => {
        const filteredProduct = [];
        for (const product of datoproducts) {

            if (product.categoria.includes(filterValues.categoria)   ) {
                filteredProduct.push(product);
                // console.log(product.title)
             }else if(product.title.includes(filterValues.title) ){

                filteredProduct.push(product);
             }
           
            
        }
        console.log(datoproducts);
        setDatofiltered(filteredProduct);
    }
    function searching(e) {
        setInfosearch(e.target.value)
        filterProducts(infosearch)

    }

    const onSubmit = data =>{  filterProducts(data)
        let inputsearch = document.getElementById('inputsearch')
        inputsearch.value=''
        console.log(data.title, data.categoria)

    }

    const handleCategoriaChange=(e)=>{
        setCategoria(e.target.options[e.target.selectedIndex].value)
    }
   
   
    return (
        <div className=" principalnav">
            <nav className="navbar navbar-expand-lg navbar-dark " >
                 <a className="navbar-brand" href="/homelogued">
                    <img src={logo} width="70" height="30" alt="" loading="lazy" />
                </a> 
                <a className="navbar-brand " href="/homelogued" tabIndex="0"><span className="nameinc ">MyTecnologic</span></a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent2" aria-controls="navbarSupportedContent2" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent2">
                    <ul className="navbar-nav mr-auto">
                        <Link to="/homelogued" className="nav-link">
                            <li className="nav-item ">
                                <span>Home</span>
                            </li>
                        </Link>
                    
                        <Link to={"/myproducts/" + usuario._id} className="nav-link">
                            <li className="nav-item">
                                {/* <span className="fas fa-user "> <span>My Products</span></span> */}
                                <img className="chico" src={chico} alt="" /><span>My Products</span>
                            </li>
                        </Link>
                        <Link to="/">
                            <AuthButton isLogged={isLogged} fnSetIsLogged={setIsLogged}></AuthButton>
                        </Link>

                        <li className="nav-item">
                        
                        </li>
                    </ul>

                    <form className="form-inline my-2 my-lg-0" onSubmit={handleSubmit(onSubmit)} id="formu2">
                    <select className="custom-select  nav-link nav-item " id="inlineFormCustomSelectPref" onChange={handleCategoriaChange} name='categoria' ref={register()}>
                        <option className="nav-link" >Categories</option>
                        <option className="nav-link" value="computing">Computing</option>
                        <option className="nav-link" value="electronics">Electronics</option>
                        <option className="nav-link" value="smartphone">SmartPhones</option>
                        <option className="nav-link" value="tools">Tools</option>
                        <option className="nav-link" value="toys">Toys</option>
                        <option className="nav-link" value="homeappliances">Homeappliances</option>
                    </select>
                        <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" onChange={searching} name="title" ref={register()}  id='inputsearch'/>
                        <button className="btn btn-outline-success my-2 my-sm-0" type="submit"  data-toggle="modal" data-target=".modal">Search</button>
                    </form>
                    {/* <Link to="/cesta" className="nav-link"> 
                        
                            <span className="spancesta">Cesta</span><span class="fas fa-cart-plus cesta"></span>
                    
                        </Link> */}
                </div>
            </nav>

            {/* modal de resultado */}

            {/*  */}
            <div className="modal" tabIndex="-1" role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title dont1">Search Result</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <h5 aria-hidden="true" className="hr">X</h5>
                            </button>
                        </div>
                        <div className="modal-body col-md-12 mb-6" >
                            <div className="cardscontainer">

                                <div className=" container row rowcardscontainer">
                                    {datofiltered[0] ?
                                        datofiltered &&
                                        datofiltered.map((product, i) => (
                                            <div key={i} className="card cardp2 col-mb-12">

                                                <a href={'/productdetail/' + product._id}>  <img
                                                    className="card-img-top imgcard2"
                                                    src={product.image}
                                                    alt="Card image cap"
                                                /></a>
                                                <ul className="list-group list-group-flush">
                                                    <li className="list-group-item1">
                                                        <span>{product.title} </span>
                                                    </li>
                                                    <li className="list-group-item">
                                                        <span className="pricecard2">
                                                            {product.price} <span className="pricecard2"> €</span>{" "}
                                                        </span>
                                                    </li>
                                                </ul>
                                                {/* cards */}
                                            </div>


                                        )) : <div> <h3 className="noresults">Sorry we have not found any match for your search </h3></div>}
                                </div>


                            </div>
                        </div>

                    </div>
                </div>
            </div>

            {/*  */}


            {/* cierre de modal resultado */}

        </div>

    );
}

export default NavComponentLogued;
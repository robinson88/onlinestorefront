import React from 'react';
import './BtnRegisterInit.scss'
const BtnRegisterInit = () => {
    return (  

        <div>
        <div className="btnregisterinit">
             <span className="spanbtnregisterinit fas fa-registered"></span>
        </div>
        <p className="nameinc">Register</p>
        </div>
    );
}

export default BtnRegisterInit;
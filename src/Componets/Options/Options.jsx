import React, { useState } from 'react';
import { Planet } from 'react-planet';
import { Link } from 'react-router-dom';
import BtnComputing from '../BtnComputing/BtnComputing';
import BtnElectronics from '../BtnElectronics/BtnElectronics';
import BtnSmartPhone from '../BtnSmartPhone/BtnSmartPhone';
import BtnTools from '../BtnTools/BtnTools';
import BtnToys from '../BtnToys/BtnToys';
import HomeAppliances from '../HomeAppliances/HomeAppliances';
import UploadProduct from '../UploadProduct/UploadProduct';

import './Options.scss'

const Options = () => {

    
    const categori = "computing"
    const categori1 = "electronics"
    const categori2 = "toys"
    const categori3 = "homeappliances"
    const categori4 = "smartphone"
    const categori5 = "tools"
    // ={
    //     cat1:'computing',
    //     cat2:'electronics',
    //     cat3:'tools',
    //     cat4:'smart phone',
    //     cat5:'home appliances',
    //     cat6:'toys'
    // }

    return (

        <div className="">
            <div className="container menucircular">
                < Planet
                    orbitStyle={defaultStyle => ({
                        ...defaultStyle,
                        borderWidth: 4,
                        borderStyle: 'dashed',
                        borderColor: '# 6f03fc',
                    })}
                    dragablePlanet
                    dragRadiusPlanet={20}
                    rebote
                    centerContent={<UploadProduct className="centro"></UploadProduct>}
                    // hideOrbit
                    autoClose
                    orbitRadius={100}
                    bounceOnClose
                    rotation={105}
                    // the bounce direction is minimal visible
                    // but on close it seems the button wobbling a bit to the bottom
                    bounceDirection="BOTTOM"
                >
                    <Link to={"/formproduct/" + categori}>
                        <BtnComputing ></BtnComputing>
                    </Link>
                    <Link to={"/formproduct/" + categori1}>
                        <BtnElectronics ></BtnElectronics>
                    </Link>
                    <Link to={"/formproduct/" + categori2}>
                        <BtnToys ></BtnToys>
                    </Link>
                    <Link to={"/formproduct/" + categori3}>
                        <HomeAppliances ></HomeAppliances>
                    </Link>
                    <Link to={"/formproduct/" + categori4}>
                        <BtnSmartPhone ></BtnSmartPhone>
                    </Link>
                    <Link to={"/formproduct/" + categori5}>
                        <BtnTools ></BtnTools>
                    </Link>


                </Planet>
            </div>

        </div>
    );
}

export default Options;
import React,{useState} from "react";
import "./FormProductLoad.scss";
import NavComponentLogued from '../../Componets/NavComponentlogued/NavComponentLogued'
import { useParams } from 'react-router-dom'
import Reloading from "../../Componets/Reloading/Reloading";
const FormProductLoad = () => {
    const [charge2, setCharge2] = useState(false)
    const categoria = useParams().categoria;
     let imgicon=""
     if(categoria=="computing"){
        imgicon="fas fa-laptop-code iconoc"
     }else if(categoria=="toys"){
        imgicon="fas fa-gamepad iconoc"
     }else if (categoria=="tools"){
        imgicon="fas fa-tools iconoc"
     }else if(categoria=="homeappliances"){
        imgicon="fas fa-tv iconoc"
     }else if(categoria=="smartphone"){
        imgicon="fas fa-mobile-alt iconoc"
     }else{
        imgicon="fas fa-atom iconoc"
     }

    console.log({ categoria })
    const usuario = JSON.parse(localStorage.getItem('usuario'));
    const onSubmit = (e) => {

        setCharge2(true)
       
        e.preventDefault();

        const datosForm = document.getElementById("dataproduct");
        const formData = new FormData(datosForm);

        console.log(formData)

        const userToken = localStorage.getItem("token");
        if (!userToken) {
            // return (window.location.href = "/Usuarios/login.html");
        }

        fetch(process.env.REACT_APP_BACK_URL + 'uploadproduct', {
            method: "POST",
            body: formData,
            headers: {
                Authorization: "Bearer " + userToken,
            },
        })
            .then((respuesta) => {
                setTimeout(() => {
                     window.location = "/homelogued"
                    document.formu.reset()
                }, 5000);
            })
            .catch((err) => {
                console.log(err);
            });
        // setTimeout('document.formu.reset()', 2000);
        return false;
    };

    return (

        <div className=" principalformproduct">
            <NavComponentLogued></NavComponentLogued>
            <div className="tapiz3 row">
                <div className="col-md-4 mb-12 containerimg03  ">
                    <div className="col-md-8 mb-12 containerimg3">
                        <div className=" interforimage">
                            {/* <img className="imguploadproduct" src={asus} alt="imagen producto" /> */}
                            <span className={imgicon}></span>
                        </div>
                    </div>
                </div>
                <div className="col-md-6 mb-12 containerfrm3 ">
                    <div className=" formregister">

                        <form
                            id="dataproduct"
                            className="formuregi3"
                            encType="multipart/form-data"
                            onSubmit={onSubmit}
                            name="formu"

                        >
                            <fieldset>
                                <legend><h3 className="textcreacuenta3">Upload Product</h3></legend>

                                {/* id usuario */}
                                <div className="col-md-12 mb-12 inputs">
                                    <input
                                        type="hidden"
                                        className="form-control "
                                        id="validationServer09"
                                        placeholder="iduser"
                                        name="iduser"

                                        value={usuario._id}
                                    />
                                    <div className="valid-feedback">
                                        Please provide a valid categoria.
                                        </div>

                                </div>

                                {/* final idusuario */}

                                <div className="col-md-12 mb-12 inputs">
                                    <input
                                        type="text"
                                        className="form-control "
                                        id="validationServer02"
                                        placeholder="Title"
                                        name="title"
                                        required
                                    />
                                    <div className="valid-feedback">
                                        Please provide a valid title.
                                        </div>
                                </div>
                                <div className="form-group col-md-12 mb-12 inputs">
                                    <label htmlFor="exampleFormControlTextarea1">Description</label>
                                    <textarea className="form-control" id="exampleFormControlTextarea1" rows="" name="description" required></textarea>
                                </div>

                                <div className="col-md-12 mb-12 inputs">
                                    <input
                                        readOnly
                                        type="text"
                                        className="form-control "
                                        id="validationServer07"
                                        placeholder="Categoria"
                                        name="categoria"
                                        value={categoria}

                                    />
                                    <div className="valid-feedback">
                                        Please provide a valid categoria.
                                        </div>

                                </div>


                                <div className="col-md-12 mb-12 inputs">
                                    <input
                                        type="text"
                                        className="form-control "
                                        id="validationServer03"
                                        placeholder="condition...Good"
                                        name="condition"
                                        required

                                    />
                                    <div className="invalid-feedback">
                                        Please provide a valid condition.
                                        </div>
                                </div>
                                <div className="col-md-12 mb-12 inputs">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="validationServer04"
                                        placeholder=" price €"
                                        name="price"
                                        required

                                    />
                                    <div className="invalid-feedback">
                                        Please provide a valid price.
                                       </div>
                                </div>
                                <div className="col-md-12 mb-12 inputs">
                                    <div className="custom-file">
                                        <input type="file" className="custom-file-input" id="customFileLang" lang="es" name="image" required />
                                        <label className="custom-file-label" htmlFor="customFileLang">Select Image</label>
                                    </div>

                                    <div className="state">
                                        <input type="hidden"  value=""/>
                                    </div>
                                </div>

                                <div className=" col ctnbtn3 inputs">
                                    {charge2? <Reloading></Reloading>:
                                    <button className="btn btn-outline-success btnup" type="submit">
                                        Upload Product
                                   </button>}
                                </div>
                            </fieldset>

                        </form>
                                   
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FormProductLoad;
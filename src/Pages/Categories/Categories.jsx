import React,{useState} from 'react';
import NavComponentLogued from '../../Componets/NavComponentlogued/NavComponentLogued';
import './Categories.scss';
import{Link} from 'react-router-dom';
const Categories = () => {
    const[categori,setCategori]=useState( {
        cat1:'computing',
        cat2:'electronics',
        cat3:'tools',
        cat4:'smart phone',
        cat5:'home appliances',
        cat6:'toys'
    })
    return ( 

        <div className="container principalcategories">
            
            <NavComponentLogued></NavComponentLogued>
            <div className="categories">
            <h4 className="categoriestext">Categories</h4>
            </div>
          
            <div className="contentitems col-md-8 mb-12">
            <Link className="nav-link linkcategories col-md-4 mb-12" to = {'/formproduct/'+ categori.cat1}>
            <div className=" iconcategorie">
            <span class="fas fa-laptop-code"></span>
            <p >Computing</p>
            </div>
            </Link>
           
            
            <Link className="nav-link linkcategories col-md-4 mb-12" to = {'/formproduct/'+ categori.cat1}>
            <div className=" iconcategorie">
            <span class="fas fa-atom"></span>
            <p>Electronics</p>
            </div>
            </Link>
            <Link className="nav-link linkcategories col-md-4 mb-12" to = {'/formproduct/'+ categori.cat1}>
            <div className=" iconcategorie">
            <span class="fas fa-tools"></span>
            <p>Tools</p>
            </div>
            </Link>

            <div className="col-md-4 mb-12 iconcategorie">
            <span class="fas fa-mobile-alt"></span>
            <p>Smart Phone</p>
            </div>
            <Link className="nav-link linkcategories col-md-4 mb-12" to = {'/formproduct/'+ categori.cat1}>
            <div className=" iconcategorie">
            <span class="fas fa-tv"></span>
            <p>Home Appliances</p>
            </div>
            </Link>
            <Link className="nav-link linkcategories col-md-4 mb-12" to = {'/formproduct/'+ categori.cat1}>
            <div className=" iconcategorie">
            <span class="fas fa-gamepad"></span>
            <p>Toys</p>
            </div>
            </Link>
            </div>
        </div>
     );
}
 
export default Categories;
import React, { useState, useEffect } from 'react';
import NavComponentLogued from '../../Componets/NavComponentlogued/NavComponentLogued';
import './MyProducts.scss'
import { useForm } from "react-hook-form";
import { API } from "../../services/Api";
import { Link, useParams } from 'react-router-dom'
import Options from '../../Componets/Options/Options';
const MyProducts = () => {

    const { register, handleSubmit } = useForm();
    const [myproducts, setMyproducts] = useState([]);
    const id = useParams().id;

    useEffect(() => {

        API.get(process.env.REACT_APP_BACK_URL + 'uploadproduct/myproducts/' + id).then(res => {

            setMyproducts(res.data)


        })

    }, [])
    // console.log(myproducts)

    ////////////////////////////////////////////////////////////

    const onSubmit = formData => {



        API.post('uploadproduct/updatestate', formData).then(res => {

            window.location = 'homelogued'

        });

    }
    const subir = (e) => {

        onSubmit()
    }
   const uploadvideo=(e)=>{
    e.preventDefault()
     
    
    const datosForm3 = document.getElementById("datavideo");
    const formData3 = new FormData(datosForm3);
    console.log(formData3)

    fetch(process.env.REACT_APP_BACK_URL + 'upvideo', {
        method: "POST",
        body: formData3,
        headers: {
            // Authorization: "Bearer " + userToken,
        },
    })
        .then((respuesta) => {
            setTimeout(() => {

                // document.formu.reset()
            }, 5000);
        })
        .catch((err) => {
            console.log(err);
        });
    // setTimeout('document.formu.reset()', 2000);
    return false;
    
    }

    /////////////////////////////////////////////////////////////////

    return (
        <div>
            <NavComponentLogued></NavComponentLogued>
            <div className="principalmyproducts">
                <div className=" textmyproducts">
                    <h3 className="welcome2">All your products</h3>
                </div>
                <div className=" containermyproducts">

                    {/* cards */}
                    <div className=" row container contcardmyproducts">
                        {myproducts[0] ?
                            myproducts.map((myproduct, i) =>


                                <div key={i} className="card  col-mb-12 cardmyproducts" >

                                    <img className="card-img-top imgmyproducts " src={myproduct.image} alt="Card image cap" />

                                    <div className="card-body">
                                    </div>

                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item1"><span > {myproduct.title} </span></li>
                                        {/* <li className="list-group-item"><span className="">100 <span > €</span> </span></li> */}



                                        <li className="list-group-item">
                                            <a href={"/editproduct/" + myproduct._id}> <span className="btn  btnedit">Edit</span> </a>
                                            <Link to={"/deleteproduct/" + myproduct._id}>
                                                <span className="btn btndelete" >Delete</span>
                                            </Link>
                                        </li>
                                        <li className="list-group-item"><span className="youcan">You can upload a short video to further detail your product</span></li>

                                        <li className="list-group-item">
                                        <Link to={"/uploadvideo/" + myproduct._id}>
                                                <span className="btn btndelete" >Upload Video</span>
                                            </Link>
                                        </li>
                                    </ul>

                                    {/* cards */}
                                </div>) : <div className="nopublic"><h1 className="textnopublic">You haven't published anything yet</h1>

                                <div className="container containerop">
                                    <Options className="optionmyproducts"></Options>
                                    <h3 className=" subtextmyproducts">Upload your Product</h3>
                                </div>
                            </div>}

                    </div>
                </div>
            </div>

            {/* modal */}


            {/* cierre modal */}
        </div>
    );
}

export default MyProducts;

{/*  */ }
import React, { useState, useEffect } from 'react';
import './DeleteProduct.scss'
import { API } from "../../services/Api";
import { Link, useParams } from 'react-router-dom'

const DeleteProduct = () => {
    const usuario = JSON.parse(localStorage.getItem('usuario'));

    const [productdetaildelete, setProductdetaildelete] = useState([]);
    const id = useParams().id;

    useEffect(() => {

        API.get(process.env.REACT_APP_BACK_URL + 'uploadproduct/myproduct/' + id).then(res => {
            
            setProductdetaildelete(res.data)


        })

    }, [])

    const Delete=()=>{
        API.post(process.env.REACT_APP_BACK_URL + 'uploadproduct/updatestate/' + id).then(res => {
            // alert("product removed")
            window.location='/myproducts/'+ usuario._id


        })
    }

    return (
        <div className="principaldelete">

            <div className="container deleteproduct">



                {/* cards */}

                <div className="">
                    <h3 className="textdelete "> Are you sure you want to remove this product</h3>
                </div>

                <button type="button" className="btn btn-primary btngo" data-toggle="modal" data-target="#exampleModal">
                    go
                </button>

            </div>

            {/*  */}

            <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">


                        </div>
                        <div className="modal-body">
                            {/*  */}
                            {productdetaildelete.map((product, i) =>

                                <div key={i} className="card cardp3 col-mb-12" >
                                    <Link to={"/productdetail/" + product._id}>
                                        <img className="card-img-top imgcard2" src={product.image} alt="Card image cap" />
                                    </Link>
                                    <div className="card-body">
                                    </div>
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item1"><span >{product.title} </span></li>
                                        <li className="list-group-item"><span className="pricecard2">{product.price} <span > €</span> </span></li>
                                        <li className="list-group-item">
                                            <span onClick={Delete} className="btn btndelete" >Delete</span>

                                        </li>
                                    </ul>
                                    {/* cards */}

                                </div>)}
                        </div>

                    </div>
                </div>
            </div>
            {/*  */}

        </div>
    );
}

export default DeleteProduct;
import React,{useState} from "react";
import "./FormUserRegister.scss";
import { API } from "../../services/Api";
import { useForm } from "react-hook-form";
import NavComponent from '../../Componets/NavComponent/NavComponent'
import Reloading from '../../Componets/Reloading/Reloading';
const FormUserRegister = () => {
    const { register, handleSubmit } = useForm();
    const [charge, setCharge] = useState(false)

    const onSubmit = (formData) => {
        setCharge(true)
        API.post("profileUser", formData).then((res) => {
            console.log("Te has registrado con exito");
            setTimeout(() => {
                window.location.href = "/login";
            }, 3000);
            
        });
    };
    return (

        <div className=" principalregister">
            <NavComponent></NavComponent>
            <div className="tapiz row">
                <div className="col-md-6 mb-12 containerimg04  ">
                    <div className=" containerimg4 ">
                        <h3 className="h3img4 hr">
                            Register and enjoy our offers
                    </h3>
                    </div>
                </div>
                <div className="col-md-6 mb-12 containerfrm  ">
                    <div className="col-md-11 formregister">

                        <form
                            className="formuregi"
                            encType="multipart/form-data"
                            onSubmit={handleSubmit(onSubmit)}
                        >
                            <fieldset>
                                <legend><h3 className="textcreacuenta">Sign up</h3></legend>
                                <div className="form-row">
                                    <div className="col-md-4 mb-3">
                                        <input
                                            type="text"
                                            className="form-control "
                                            id="validationServer01"
                                            placeholder="Name"
                                            name="name"
                                            ref={register({ required: true })}
                                        />
                                        <div className="valid-feedback">
                                            Please provide a valid name.
                                        </div>
                                    </div>
                                    <div className="col-md-4 mb-3">
                                        <input
                                            type="email"
                                            className="form-control "
                                            id="validationServer02"
                                            placeholder="email"
                                            name="email"
                                            ref={register({ required: true })}
                                        />
                                        <div className="valid-feedback">
                                            Please provide a valid email.
                                        </div>
                                    </div>

                                    <div className="col-md-4 mb-3">
                                        <input
                                            type="password"
                                            className="form-control "
                                            id="validationServer07"
                                            placeholder="password"
                                            name="pass"
                                            ref={register({ required: true })}
                                        />
                                        <div className="valid-feedback">
                                            Please provide a valid password.
                                        </div>
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="col-md-4 mb-3">
                                        <input
                                            type="text"
                                            className="form-control "
                                            id="validationServer03"
                                            placeholder="phone"
                                            name="phone"
                                            ref={register({ required: true })}
                                        />
                                        <div className="invalid-feedback">
                                            Please provide a valid phone.
                                        </div>
                                    </div>
                                    <div className="col-md-4 mb-3">
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="validationServer04"
                                            placeholder="address"
                                            name="address"
                                            ref={register({ required: true })}
                                        />
                                        <div className="invalid-feedback">
                                            Please provide a valid address.
        </div>
                                    </div>
                                    <div className="col-md-4 mb-3">
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="validationServer05"
                                            placeholder="postal code"
                                            name="postalcode"
                                            ref={register({ required: true })}
                                        />
                                        <div className="invalid-feedback">
                                            Please provide a valid postal code.
        </div>
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="col-md-6 mb-3">
                                        <input
                                            type="text"
                                            className="form-control "
                                            id="validationServer06"
                                            placeholder="city"
                                            name="city"
                                            ref={register({ required: true })}
                                        />
                                        <div className="invalid-feedback">
                                            Please provide a valid phone.
        </div>
                                    </div>
                                    {/* <div className="col-md-6 mb-3">
                                        <div className="custom-file">
                                            <input type="file" className="custom-file-input" id="customFileLang" lang="es" ref={register({ required: true })} />
                                            <label className="custom-file-label" for="customFileLang">Select Image</label>
                                        </div>
                                    </div> */}
                                </div>
                                {charge? <Reloading></Reloading>:
                                <div className=" ctnbtn">
                                    
                                    <button className="btn btn-outline-success" type="submit">
                                        Register
    </button>
                                </div>}
                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FormUserRegister;


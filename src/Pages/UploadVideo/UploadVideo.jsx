
import React, { useState, useEffect } from 'react';
import NavComponentLogued from '../../Componets/NavComponentlogued/NavComponentLogued';
import './UploadVideo.scss';
import { API } from "../../services/Api";
import { useParams } from 'react-router-dom'
import 'jquery'
import Reloading from '../../Componets/Reloading/Reloading';

const UploadVideo = () => {
    const [charge, setCharge] = useState(false)
    const [productdetail, setProductdetail] = useState([]);
    const [userdetail, setUserdetail] = useState([]);
    const id = useParams().id;
    const [idproduct, setIdproduct] = useState('');
    const [vvideo, setVvideo] = useState('');

    useEffect(() => {

        API.get(process.env.REACT_APP_BACK_URL + 'uploadproduct/' + id).then(res => {

            setProductdetail(res.data[0])
            setIdproduct(id)

        })

    }, [])
    let idproduct1 = idproduct

    const datovende = () => {

        API.get(process.env.REACT_APP_BACK_URL + 'profileUser/' + productdetail.iduser).then(res => {

            setUserdetail(res.data[0]);

        })


    }
    const onSubmit = (e) => {
        setCharge(true)
        e.preventDefault();

        const datosForm3 = document.getElementById("datavideo");
        const formData3 = new FormData(datosForm3);
        console.log(formData3)

        fetch(process.env.REACT_APP_BACK_URL + 'upvideo', {
            method: "POST",
            body: formData3,
            headers: {
                // Authorization: "Bearer " + userToken,
            },
        })
            .then((respuesta) => {
                setTimeout(() => {
                   
                    setCharge(false)
                    viewvideo()
                    // window.location='/myproducts/'+productdetail.iduser
                }, 3000);
            })
            .catch((err) => {
                console.log(err);
            });
        // setTimeout('document.formu.reset()', 2000);
        return false;
    }
    const viewvideo=()=>{
        API.get(process.env.REACT_APP_BACK_URL + 'upvideo?id='+id).then(res => {

            setVvideo(res.data[0]);

        })

    }

    return (
        <div className="principalproductdetail">
            <NavComponentLogued></NavComponentLogued>




            <div class="container">
                <div class="text"></div>
            </div>
            <div className="container containerdetail">
                <div className=" container row rowdetailproduct">
                    <div className="col-md-6 mb-12 imagedetail" >
                        <div className="contimgdetail">
                            <img className="imgdetail" src={productdetail.image} alt="" />

                        </div>
                        <div>
                            <form id="datavideo" className="formuregi3" encType="multipart/form-data" onSubmit={onSubmit} name="formu">
                                <div className="custom-file">
                                    <input type="file" className="custom-file-input" lang="es" name="video" required />
                                    <input type="hidden" name="idproduct" value={id} />
                                    <label className="custom-file-label" htmlFor="customFileLang">Select video</label>
                                    {charge?<Reloading></Reloading>:
                                    <button className="btn btnvideo  " type="submit" > Upload video</button>}
                                </div>
                            </form>


                        </div>



                    </div>
                    <div className="col-md-6 mb-12 textdetail" >
                        <div className="detailproduct2">
                           <video className="videomyproduct" src={vvideo.video} controls></video>
                        </div>
                    </div>
                </div>



            </div>
            <div className="final">

            </div>

        </div>
    );
}

export default UploadVideo;
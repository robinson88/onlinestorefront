import React, { useState, useEffect } from "react";
import NavComponentLogued from "../../Componets/NavComponentlogued/NavComponentLogued";
import { Link } from "react-router-dom";
import "./HomeLogued.scss";
import $ from 'jquery'
import Options from "../../Componets/Options/Options";
import { API } from "../../services/Api";
const HomeLogued = () => {
    const [datoproduct, setDatoproduct] = useState([]);
    const usuario = JSON.parse(localStorage.getItem("usuario"));

    useEffect(() => {
        API.get(process.env.REACT_APP_BACK_URL + "uploadproduct").then((res) => {
            setDatoproduct(res.data);
        });
    }, []);

    var
        words = [`Welcome ${usuario.name}`],
        part,
        i = 0,
        offset = 0,
        len = words.length,
        forwards = true,
        skip_count = 0,
        skip_delay = 5,
        speed = 150;

    var wordflick = function () {
        setInterval(function () {
            if (forwards) {
                if (offset >= words[i].length) {
                    ++skip_count;
                    // if (skip_count == skip_delay) {
                    //   forwards = false;
                    //   skip_count = 0;
                    // }
                }
            }
            else {
                if (offset == 0) {
                    forwards = true;
                    i++;
                    offset = 0;
                    //   if(i >= len){
                    //     i=0;
                    //   } 
                }
            }
            part = words[i].substr(0, offset);
            if (skip_count == 0) {
                if (forwards) {
                    offset++;
                }
                else {
                    offset--;
                }
            }
            $('.word').text(part);
        }, speed);
    };

    wordflick();

    return (
        <div>
            <NavComponentLogued></NavComponentLogued>
            <div className=" principalhomelogued">

                <div className="animation">
                    <h1 className="welcome word hr"> </h1>
                </div><br /><br /><br />



                {/* carrusel */}
                <div
                    id="carouselExampleIndicators"
                    className="carousel "
                    data-ride="carousel"
                >
                    <ol className="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"> </li>
                    </ol>
                     <div className="carousel-inner">
                         <div className="carousel-item active">
                         <iframe  width="560" height="315" src="https://www.youtube.com/embed/yq_0He8hOGM"
                          frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div className="carousel-item">
                    <iframe  width="560" height="315" src="https://www.youtube.com/embed/RkC0l4iekYo"
                     frameborder="0"
                      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div> 
                    <div className="carousel-item ">
                        <iframe  width="560" height="315" src="https://www.youtube.com/embed/0h4_sJbnRsI"
                             frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>

                        </iframe>
                    </div>
                    <div className="carousel-item ">
                        <iframe
                            width="560" height="315"
                            src="https://www.youtube.com/embed/cnXapYkboRQ"
                            frameborder="0"
                            allow="autoplay; encrypted-media"
                        // allowfullscreen
                        ></iframe>
                    </div>
                </div>
                <a
                    className="carousel-control-prev"
                    href="#carouselExampleIndicators"
                    role="button"
                    data-slide="prev"
                >
                    <span
                        className="carousel-control-prev-icon"
                        aria-hidden="true"
                    ></span>
                    <span className="sr-only">Previous</span>
                </a>
                <a
                    className="carousel-control-next"
                    href="#carouselExampleIndicators"
                    role="button"
                    data-slide="next"
                >
                    <span
                        className="carousel-control-next-icon"
                        aria-hidden="true"
                    ></span>
                    <span className="sr-only">Next</span>
                </a>
            </div>
            {/* carrusel */}
            <div className="buttonupproducts md-6 mb-12">
                <h5 className="textoprincipal ">
                    Do you want to sell Earn money, it's easy just upload your product?
                </h5>
                {/* <Link className="linkhomelogued" to="/categories"> */}

                {/* </Link> */}
            </div>
            <div className=" container col-md-12 mb-12 containerofop">
                <Options className="desplieguemenu"></Options>
                <div>
                    <span>
                        sell now</span>
                </div>

            </div>
            <div className="separator">
                <h2 className="textnews">Latest news</h2>
                <h4 className="dont  ">Don't miss our latest news and promotions</h4>
            </div>
            <div className="cardscontainer">
                {/* cards */}
                <div className=" container row rowcardscontainer">
                    {datoproduct &&
                        datoproduct.map((product, i) => (
                            <div key={i} className="card cardp2 col-mb-12">
                                <Link to={"/productdetail/" + product._id}>
                                    <img
                                        className="card-img-top imgcard2"
                                        src={product.image}
                                        alt="Card image cap"
                                    />
                                </Link>
                                <div className="card-body">
                                    {/* <h5 className="card-title">{product.title} </h5> */}

                                    {/*  */}
                                </div>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item1">
                                        <span>{product.title} </span>
                                    </li>
                                    <li className="list-group-item">
                                        <span className="pricecard2">
                                            {product.price} <span className="pricecard2"> €</span>{" "}
                                        </span>
                                    </li>
                                </ul>
                                <i class="fas fa-video"></i>
                                {/* cards */}
                            </div>
                        ))}
                </div>
            </div>
        </div>
        </div >
    );
};

export default HomeLogued;

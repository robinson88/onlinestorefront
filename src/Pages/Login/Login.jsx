import React,{useState} from 'react';
import { API } from '../../services/Api';
import { useForm } from "react-hook-form";
import NavComponent from '../../Componets/NavComponent/NavComponent'
import './Login.scss';
import Reloading from '../../Componets/Reloading/Reloading';


const Login = (props) => {

    const [charge, setCharge] = useState(false)

    const { register, handleSubmit } = useForm();


    const onSubmit = formData => {
        setCharge(true)
        API.post('profileUser/login', formData).then(res => {

            localStorage.setItem('token', res.data.token)
            localStorage.setItem('usuario', JSON.stringify(res.data.usuario))
            props.fnSetIsLogged(true);
            
            setTimeout(() => {
                window.location.href = "/homelogued";
            }, 3000);
            
        })
    }
    return (


        <div className=" principallogin">
            <NavComponent></NavComponent>
            <div className="  tapiz row">
                <div className="col-md-5 mb-12 containerimg20">
                    <div className=" containerimg2 ">

                        {/* <video className="videologin"src="" controls></video> */}
                        <h3 className="h3img hr">
                            if you are looking for technology you are in the right place
                        </h3>
                    </div>
                </div>
                <div className="col-xs-12 col-sm-12  col-md-6   containerfrm2 container">
                    <div className="container col-md-6 formregister2">

                        <form
                            className="formuregi"
                            encType="multipart/form-data"
                            onSubmit={handleSubmit(onSubmit)}
                        >
                            <fieldset >
                                <legend><h3 className="textcreacuenta2">Login</h3></legend>
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1">Email address</label>
                                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="email" ref={register({ required: true })} />
                                    <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="exampleInputPassword1">Password</label>
                                    <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" name="pass" ref={register({ required: true })} />
                                </div>
                                    {charge?<Reloading></Reloading>:
                                <button type="submit" className="btn btn-outline-success">Login</button>}
                                <p className="createacount">If you don't have an account yet, create one <a className="createacount" href="/register">HERE</a> </p>
                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>




    );
}

export default Login;



// 
import React, { useState, useEffect } from 'react';
import { API } from "../../services/Api";
import { useParams } from 'react-router-dom'
import './EditProduct.scss'
import Reloading from '../../Componets/Reloading/Reloading';
import NavComponentLogued from '../../Componets/NavComponentlogued/NavComponentLogued';
const EditProduct = () => {
    const usuario = JSON.parse(localStorage.getItem('usuario'));
    const [productdetail3, setProductdetail3] = useState([]);
    const id = useParams().id;
    const [charge, setCharge] = useState(false)


    useEffect(() => {

        API.get(process.env.REACT_APP_BACK_URL + 'uploadproduct/' + id).then(res => {

            setProductdetail3(res.data[0])


        })

    }, [])


    const onSubmit = (e) => {
        setCharge(true)


        // ¿ Para que no te lleve a otro sitio ?
        e.preventDefault();

        const datosForm = document.getElementById("dataproduct");
        const formData = new FormData(datosForm)

        console.log(formData)

        const userToken = localStorage.getItem("token");
        if (!userToken) {
            // return (window.location.href = "/Usuarios/login.html");
        }

        fetch(process.env.REACT_APP_BACK_URL + 'uploadproduct/edit/' + id, {
            method: "POST",
            body: formData,
            headers: {
                Authorization: "Bearer " + userToken,
            },
        })
            .then((respuesta) => {
                setTimeout(() => {

                    window.location = "/homelogued"
                    document.formu.reset()
                }, 5000);

            })
            .catch((err) => {
                console.log(err);
            });
        // setTimeout('document.formu.reset()', 2000);
        return false;

    }




    return (
        <div className="principaleditproduct">

            <NavComponentLogued></NavComponentLogued>
            <div className="tapiz5 row">
                
                <div className="col-md-3 mb-12">
                    
                        
                            {/* <img className="imguploadproduct" src={productdetail3.image} alt="imagen producto" /><br />
                            <hr className />
                            <p className="pedit"> {productdetail3.title}</p>
                            <p className="pedit">{productdetail3.description}</p>
                            <p className="pedit">{productdetail3.condition}</p>
                            <p className="pedit">{productdetail3.price}€</p> */}
                            <div class="card  card7" >
                                <img src= {productdetail3.image}class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <h5 class="card-title">{productdetail3.title}</h5>
                                    <p class="card-text">{productdetail3.description}</p>
                                    <p class="card-text">{productdetail3.condition}</p>
                                    <h4 class="card-text">{productdetail3.price}€</h4>
                                   
                                </div>
                            </div>
                        
                    
                </div>
                <div className="col-md-5 mb-12 containerfrm3 ">
                    <div className=" formregister">


                        <form
                            id="dataproduct"
                            className="formuregi3"
                            encType="multipart/form-data"
                            onSubmit={onSubmit}
                            name="formu"

                        >
                            <fieldset>
                                <legend><h3 className="textcreacuenta3">Edit Product</h3></legend>

                                {/* id usuario */}
                                <div className="col-md-12 mb-12 inputs">
                                    <input
                                        type="hidden"
                                        className="form-control "
                                        id="validationServer09"
                                        placeholder="iduser"
                                        name="iduser"
                                        value={usuario._id}
                                    />
                                    <div className="valid-feedback">
                                        Please provide a valid categoria.
                                        </div>

                                </div>

                                {/* final idusuario */}

                                <div className="col-md-12 mb-12 inputs">
                                    <input
                                        type="text"
                                        className="form-control "
                                        id="validationServer02"
                                        placeholder="Title"
                                        name="title"
                                        required



                                    />
                                    <div className="valid-feedback">
                                        Please provide a valid title.
                                        </div>
                                </div>
                                <div className="form-group col-md-12 mb-12 inputs4">
                                    <label htmlFor="exampleFormControlTextarea1">Description</label>
                                    <textarea className="form-control" id="exampleFormControlTextarea1" rows="" name="description" required></textarea>
                                </div>

                                {/* <div className="col-md-12 mb-12 inputs4">
                                    <input
                                        readOnly
                                        type="text"
                                        className="form-control "
                                        id="validationServer07"
                                        placeholder="Categoria"
                                        name="categoria"


                                    />
                                    <div className="valid-feedback">
                                        Please provide a valid categoria.
                                        </div>

                                </div> */}


                                <div className="col-md-12 mb-12 inputs4">
                                    <input
                                        type="text"
                                        className="form-control "
                                        id="validationServer03"
                                        placeholder="condition...Good"
                                        name="condition"
                                        required

                                    />
                                    <div className="invalid-feedback">
                                        Please provide a valid condition.
                                        </div>
                                </div>
                                <div className="col-md-12 mb-12 inputs4">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="validationServer04"
                                        placeholder=" price €"
                                        name="price"
                                        required


                                    />
                                    <div className="invalid-feedback">
                                        Please provide a valid price.
                                       </div>
                                </div>
                                <div className="col-md-12 mb-12 inputs4">
                                    <div className="custom-file">
                                        <input type="file" className="custom-file-input" id="customFileLang" lang="es" name="image" required />
                                        <label className="custom-file-label" htmlFor="customFileLang">Select Image</label>
                                    </div>

                                    <div className="state">
                                        <input type="hidden" value="" />
                                    </div>
                                </div>

                                <div className=" col ctnbtn3 inputs">
                                    {charge ?
                                        <Reloading className="loadload"></Reloading> :
                                        <button className="btn btn-outline-success btnup" type="submit">
                                            Edit Product
                                   </button>}
                                </div>
                            </fieldset>

                        </form>

                    </div>
                </div>
            </div>

        </div>

    );
}

export default EditProduct;
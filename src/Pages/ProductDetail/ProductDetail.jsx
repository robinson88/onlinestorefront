import React, { useState, useEffect } from 'react';
import NavComponentLogued from '../../Componets/NavComponentlogued/NavComponentLogued';
import './ProductDetail.scss';
import { API } from "../../services/Api";
import { useParams } from 'react-router-dom'
import 'jquery'
const ProductDetail = () => {

    const [productdetail, setProductdetail] = useState([]);
    const [userdetail, setUserdetail] = useState([]);
    const id = useParams().id;
    const [idproduct, setIdproduct] = useState('');

    useEffect(() => {

        API.get(process.env.REACT_APP_BACK_URL + 'uploadproduct/' + id).then(res => {

            setProductdetail(res.data[0])
            setIdproduct(id)

        })

    }, [])
    let idproduct1 = idproduct

    const datovende = () => {

        API.get(process.env.REACT_APP_BACK_URL + 'profileUser/' + productdetail.iduser).then(res => {

            setUserdetail(res.data[0]);

        })


    }
    const onSubmit = (e) => {

        e.preventDefault();

        const datosForm3 = document.getElementById("datavideo");
        const formData3 = new FormData(datosForm3);
        console.log(formData3)

        fetch(process.env.REACT_APP_BACK_URL + 'upvideo', {
            method: "POST",
            body: formData3,
            headers: {
                // Authorization: "Bearer " + userToken,
            },
        })
            .then((respuesta) => {
                setTimeout(() => {

                    document.formu.reset()
                }, 5000);
            })
            .catch((err) => {
                console.log(err);
            });
        // setTimeout('document.formu.reset()', 2000);
        return false;
    }
    /////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////

    return (

        <div className="principalproductdetail">
            <NavComponentLogued></NavComponentLogued>




            <div class="container">
                <div class="text"></div>
            </div>
            <div className="container containerdetail">
                <div className=" container row rowdetailproduct">
                    <div className="col-md-6 mb-12 imagedetail" >
                        <div className="contimgdetail">
                            <img className="imgdetail" src={productdetail.image} alt="" />

                        </div>
                    </div>
                    <div className="col-md-6 mb-12 textdetail" >
                        <div className="detailproduct2">
                            <h4>{productdetail.title}</h4><br />

                            <p>{productdetail.description}.</p>
                            <h4>{productdetail.price}€</h4>
                            <button className="btn btn-outline-success" onClick={datovende}>see seller data</button><br /><br />
                            <span>Seller:  </span><span>{userdetail.name}</span><br />
                            <span>Phone:   </span><span>{userdetail.phone}</span><a href={`https://api.whatsapp.com/send?phone=+34${userdetail.phone}&text=`}><span class="fab fa-whatsapp-square wapp"></span></a>
                        </div>
                    </div>
                </div>



            </div>
            <div className="final">

            </div>

        </div>
    );
}

export default ProductDetail;
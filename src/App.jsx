
import React,{useState} from 'react';
import './App.scss';
import 'bootstrap/dist/css/bootstrap.min.css'
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import FormUserRegister from './Pages/FormUserRegister/FormUserRegister';
import Home from './Pages/Home/Home';
import Login from './Pages/Login/Login';
import HomeLogued from './Pages/HomeLogued/HomeLogued';
import Cheap from './Pages/Cheap/Cheap';
import Cesta from './Pages/Cesta/cesta';
import Categories from './Pages/Categories/Categories';
import FormProductLoad from './Pages/FormLoadProduct/FormProductLoad';
import ProductDetail from './Pages/ProductDetail/ProductDetail';
import Options from './Componets/Options/Options';
import MyProducts from './Pages/MyProducts/MyProducts';
import DeleteProduct from './Pages/DeleteProduct/DeleteProduct';
import EditProduct from './Pages/EditProduct/EditProduct';
import UploadVideo from './Pages/UploadVideo/UploadVideo';
function App() {

  const [isLogged, setIsLogged] = useState(!!localStorage.getItem('token'));
  return (
    <div className="App">
      
      {/* <NavComponent></NavComponent> */}
      <Router>
        <Switch>
          <Route path="/register">
            <FormUserRegister></FormUserRegister>
          </Route>
          <Route path="/options">
            <Options></Options>
          </Route>

          <Route path="/formproduct/:categoria">
            <FormProductLoad></FormProductLoad>
          </Route>
          <Route path="/productdetail/:id">
            <ProductDetail></ProductDetail>
          </Route>

          <Route path="/deleteproduct/:id">
            <DeleteProduct></DeleteProduct>
          </Route>

          <Route path="/editproduct/:id">
            <EditProduct></EditProduct>
          </Route>

          <Route path="/login">
            <Login fnSetIsLogged={setIsLogged}></Login>
          </Route>
          <Route path="/myproducts/:id">
          {isLogged && <MyProducts isLogged={isLogged} />}
          </Route>
          <Route path="/uploadvideo/:id">
          {isLogged && <UploadVideo isLogged={isLogged} />}
          </Route>

          <Route path="/categories">
           <Categories></Categories>
          </Route>

          <Route path="/cheap">
          {isLogged && <Cheap isLogged={isLogged} />}
          </Route>

          <Route path="/homelogued">
            <HomeLogued></HomeLogued>
          </Route>
          <Route path="/cesta">
            <Cesta></Cesta>
          </Route>

          <Route path="/">
            <Home></Home>
          </Route>

        </Switch>
      </Router>
    </div>
  );
}

export default App;
